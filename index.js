const express = require('express');
const app = express();
const cookieParser = require('cookie-parser');
const cors = require('cors');
const bodyParser = require('body-parser');
var order = 0;
var gameId = '101';
var gameChannel = 'presence-game' + gameId + '-channel';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.json());
app.use(cookieParser());
app.use(cors());
app.use(express.static('public'));
app.listen(process.env.PORT || 3000);

const Pusher = require('pusher');
const { request } = require('express');
const pusher = new Pusher({
	appId: '1064951',
	key: '47cce9c74d6dd840dac6',
	secret: '06762d571a0b39f6af2d',
	cluster: 'eu',
	forceTLS: true
});

app.post('/pusher/auth', function (req, res) {
	const socketId = req.body.socket_id;
	const channel = req.body.channel_name;
	const userId = req.cookies.user_id;
	const unique = new Date().valueOf().toString(36) + Math.random().toString(36).substr(2);
	order += 1;
	const presenceData = {
		user_id: userId,
		user_info: {
			coins: [],
			order: order,
			remaining: null,
			rounds: [{
				duration: null,
				start: null
			}, {
				duration: null,
				start: null
			}, {
				duration: null,
				start: null
			}, {
				duration: null,
				start: null
			}],
			unique: unique
		}
	};
	const auth = pusher.authenticate(socketId, channel, presenceData);
	res.send(auth);
});

//trigger the coin braodcast message
function triggerCoin(coin, unique) {
	pusher.trigger(gameChannel, 'coin-event', {
		message: 'Coin event',
		coin: coin,
		unique: unique
	});
}

function coinsByState(coins, boolean) {
	var result = coins.filter(function (coin) {
		return coin.selected === boolean;
	});
	return result;
}

function resetCoins(coins) {
	var result = coins.filter(function (coin) {
		coin.selected = false;
		return coin;
	});
	return result;
}

function calculateDuration(stop, start) {
	var duration = (new Date(stop) - new Date(start)) / 1000;
	return parseFloat(duration).toFixed(2);
}

//trigger the batch braodcast message
function triggerBatch(batches, round, fromPlayer, toPlayer) {
	var duration = null;	
	var coins = fromPlayer.info.coins;
	var gameTotal = batches[0];
	var passedCoins = coinsByState(coins, true);
	var unusedCoins = coinsByState(coins, false)
	var time = new Date().toISOString(); //consistent server time
	
	//first pass from player to the next player [start time]
	if (fromPlayer.info.remaining === gameTotal && toPlayer.info.unique !== 'customer') {
		toPlayer.info.rounds[round].start = time;
	}

	fromPlayer.info.coins = unusedCoins; //retain unused coins
	fromPlayer.info.remaining -= passedCoins.length; //reduce number of coins remaining

	//no coins remain from player [calculate player duration]
	if (fromPlayer.info.remaining === 0) {
		duration = calculateDuration(time, fromPlayer.info.rounds[round].start);
		fromPlayer.info.rounds[round].duration = duration;

		//all to customer (calculate customer duration)
		if (toPlayer.info.unique === 'customer') {			
			duration = calculateDuration(time, toPlayer.info.rounds[round].start);
			toPlayer.info.rounds[round].duration = duration;
		}
	}

	//first customer coins (calculate first batch duration)
	if (toPlayer.info.unique === 'customer' && fromPlayer.info.remaining === (gameTotal - passedCoins.length)) {
		duration = calculateDuration(time, toPlayer.info.rounds[round].start);
		toPlayer.info.rounds[round].first = duration;
	}

	//reset coins state for next player to receive
	toPlayer.info.coins = resetCoins(passedCoins);

	pusher.trigger(gameChannel, 'batch-event', {
		message: 'Batch Event Sent',
		from_player: fromPlayer,
		to_player: toPlayer
	});
}

function triggerRoundStart(round, start) {
	pusher.trigger(gameChannel, 'round-started-event', {
		message: 'Round Start',
		start: start,
		round: round
	});
}

//define rate-limited function calls
var coinFlip = rateLimit(triggerCoin, 100);
var batchPass = rateLimit(triggerBatch, 100);

//when a coin is sent
app.post('/coinFlip', function (req, res) {
	var coin = req.body.coin;
	var unique = req.body.unique;
	var json = {
		message: 'Server responded to route /coinFlip'
	};
	res.status('200').send(json);
	coinFlip(coin, unique);
});

//when a batch is sent
app.post('/passBatch', function (req, res) {
	var batches = req.body.batches;
	var round = req.body.round;
	var fromPlayer = req.body.from_player;
	var toPlayer = req.body.to_player;
	var json = {
		message: 'Server responded to route /passBatch'
	};
	res.status('200').send(json);
	batchPass(batches, round, fromPlayer, toPlayer);
});

//when a round start is received
app.post('/triggerRoundStart', function (req, res) {
	var round = parseInt(req.body.round, 0);
	var start = new Date().toISOString();
	var json = {
		message: 'Server responded to route /triggerRoundStart'
	};
	triggerRoundStart(round, start);
	res.status('200').send(json);
});

// rate limiter...
// https://stackoverflow.com/questions/23072815/throttle-javascript-function-calls-but-with-queuing-dont-discard-calls
// http://jsfiddle.net/dandv/47cbj/
function rateLimit(fn, delay, context) {
	var queue = [], timer = null;

	function processQueue() {
		var item = queue.shift();
		if (item)
			fn.apply(item.context, item.arguments);
		if (queue.length === 0)
			clearInterval(timer), timer = null;
	}

	return function limited() {
		queue.push({
			context: context || this,
			arguments: [].slice.call(arguments)
		});
		if (!timer) {
			processQueue();  // start immediately on the first invocation
			timer = setInterval(processQueue, delay);
		}
	}

}